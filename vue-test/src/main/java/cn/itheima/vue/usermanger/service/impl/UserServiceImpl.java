package cn.itheima.vue.usermanger.service.impl;

import cn.itheima.vue.usermanger.entity.PageQueryBean;
import cn.itheima.vue.usermanger.mapper.UserMapper;
import cn.itheima.vue.usermanger.pojo.User;
import cn.itheima.vue.usermanger.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

/*
    @Override
    public List<User> findAll() {
        List<User> users=userMapper.findAll();
        return users;
    }
*/

    //Mybatis插件分页
    @Override
    public Map pageQuery(PageQueryBean pageQueryBean) {
        //分页参数 传递 PageHelper
        PageHelper.startPage(pageQueryBean.getPageNum(),
                pageQueryBean.getPageSize());
        List<User> userlist = userMapper.findAll(pageQueryBean.getQueryString());
        PageInfo<User> pageInfo = new PageInfo<>(userlist);
        Map map = new HashMap();
        map.put("userlist",pageInfo.getList());
        map.put("total",pageInfo.getTotal());
        return map;
    }

    @Override
    public Integer deleteUserById(Integer id) {
        return userMapper.deleteUserById(id);
    }




    /*//原始分页
    @Override
    public Map pageQuery(Integer pageNum, Integer pageSize) {
        List<User> userlist  =  userMapper.pageQuery((pageNum-1)*pageSize,pageSize);
        // select * from  xx limit  ?,?
        long  total  = userMapper.findTotalCounts();
        //  select count(1) from  xxx
        Map map = new HashMap();
        map.put("userlist",userlist);
        map.put("total",total);
        return map;
    }*/
}
