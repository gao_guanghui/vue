package cn.itheima.vue.usermanger.mapper;

import cn.itheima.vue.usermanger.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    List<User> findAll(@Param("pageQueryString") String pageQueryString);

    List<User> pageQuery(@Param("startIndex") int startIndex,@Param("pageSize") Integer pageSize);

    long findTotalCounts();

    Integer deleteUserById(Integer id);

}
