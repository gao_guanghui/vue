package cn.itheima.vue.usermanger.controller;

import cn.itheima.vue.usermanger.entity.PageQueryBean;
import cn.itheima.vue.usermanger.pojo.User;
import cn.itheima.vue.usermanger.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserCotroller {
  //修改测试一
    @Autowired
    private UserService userService;

    /*@RequestMapping("/findAll.do")
    public List<User> findAll(){
        List<User> userList = userService.findAll();
        return userList;
    }
*/
    //分页查询
    @RequestMapping("/pageQuery.do")
    public Map pageQuery(@RequestBody PageQueryBean pageQueryBean){
        Map map = userService.pageQuery(pageQueryBean);
        return map;
    }

    //删除
    @RequestMapping("/deleteUserById.do")
    public Map deleteUserById(@RequestParam("id") Integer id){
        userService.deleteUserById(id);
        return null;


    }
  /*  @RequestMapping("/pageQuery.do")
    public Map pageQuery(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize){
        Map map = userService.pageQuery(pageNum,pageSize);
        return map;
    }*/
}
