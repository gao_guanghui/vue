package cn.itheima.vue.usermanger.service;

import cn.itheima.vue.usermanger.entity.PageQueryBean;
import cn.itheima.vue.usermanger.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserService {

  //  public List<User> findAll();

    Map pageQuery(PageQueryBean pageQueryBean);

    Integer deleteUserById(Integer id);

    // Map pageQuery(Integer pageNum, Integer pageSize);
}
